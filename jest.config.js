module.exports = {
  preset: 'react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transformIgnorePatterns: [
    'node_modules/?!(static-container)',
    'node_modules/(?!(@react-native|react-native|@sentry/react-native)/)',
  ],
  setupFiles: ['./jest-setup.js'],
};
