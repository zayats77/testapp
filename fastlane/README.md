fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## Android

### android build_bundle

```sh
[bundle exec] fastlane android build_bundle
```

Build signed bundle

### android build_apk

```sh
[bundle exec] fastlane android build_apk
```



### android upload_sentry

```sh
[bundle exec] fastlane android upload_sentry
```

Upload source maps

### android upload_internal

```sh
[bundle exec] fastlane android upload_internal
```

Upload to internal test

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
